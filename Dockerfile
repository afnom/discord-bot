FROM python:3.9
ARG user=web

RUN apt-get update && \
    apt-get upgrade -y
RUN apt-get install -y wait-for-it

RUN useradd -s /sbin/nologin $user

WORKDIR /usr/src/app

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY *.py ./

USER $user
CMD wait-for-it $DB_HOST:$DB_PORT -- python -u user_bot.py

