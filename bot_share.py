import yaml
import os

DISCORD_CONFIG = yaml.safe_load(open("discord_config.yaml"))
POLL_FREQ = DISCORD_CONFIG['poll_freq']
ERRORS_CHANNEL = DISCORD_CONFIG['errors_channel']
ANNOUNCEMENTS_CHANNEL = DISCORD_CONFIG['announcements_channel']
GUILD = os.getenv('DISCORD_GUILD')
