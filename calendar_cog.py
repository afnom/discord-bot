## For Google calendar
from __future__ import print_function
import datetime
from dateutil import parser as dateutil_parser
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
import os
from socket import gaierror
import pickle

import logging
import discord
from discord.ext import commands, tasks

logging.basicConfig(level=logging.INFO)
logging.getLogger('googleapiclient.discovery_cache').setLevel(logging.ERROR)

from bot_share import POLL_FREQ, ERRORS_CHANNEL, ANNOUNCEMENTS_CHANNEL
SCOPES = ['https://www.googleapis.com/auth/calendar.readonly']
CAL_EVENTS_FILE = os.getenv('CAL_EVENTS_FILE')
CAL_CREDS = os.getenv('CAL_CREDS')
CAL_TOKEN = os.getenv('CAL_TOKEN')

class CalendarCog(commands.Cog, name='Calendar'):
    def __init__(self, bot):
        self.bot = bot
        self.logger = logging.getLogger('calendar')
        self.logger.info("Added CalendarCog")
        self.logger.info("Poll freq: " + str(POLL_FREQ))

    @commands.Cog.listener()
    async def on_ready(self):
        self.check_calendar.start()
        self.logger.info("Started calendar check")


    async def send_calendar_embed(self, event, title):
        start = event['start'].get('dateTime', event['start'].get('date'))
        end = event['end'].get('dateTime', event['end'].get('date'))
        start_time_obj = dateutil_parser.parse(start) 
        end_time_obj = dateutil_parser.parse(end) 
        new_event_embed = discord.Embed(title=event['summary'], url=event['htmlLink'], description=f'From {start_time_obj.strftime("%a, %d %b %Y, %H:%M")} to {end_time_obj.strftime("%a, %d %b %Y, %H:%M")}')
        channel = self.bot.get_channel(ANNOUNCEMENTS_CHANNEL)
        msg = await channel.send(":calendar: " + title, embed=new_event_embed)
        return msg 

    @tasks.loop(minutes=POLL_FREQ)
    async def check_calendar(self):
        saved_events = pickle.load(open(CAL_EVENTS_FILE, 'rb'))

        try:
            events = self.get_remote_events(10) 
        except gaierror as e:
            channel = self.bot.get_channel(ERRORS_CHANNEL)
            msg = await channel.send("Error: " + str(e))
        now = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
        if events == -1:
            channel = self.bot.get_channel(ERRORS_CHANNEL)
            await channel.send("Calendar tokens need to be refreshed!")
            return False

        to_del = []
        to_add = []

        ## remove old events 
        for ev in saved_events:
            _found = False
            for event in events:
                if ev['id'] == event['id']:
                    _found = True
            if not _found:
                print("Event deleted", ev['summary'])
                to_del.append(ev)

        for ev in to_del:
            saved_events.remove(ev)

        to_del = []

        for event in events:
            _saved = False
            start = event['start'].get('dateTime', event['start'].get('date'))
            for ev in saved_events:
                if ev['id'] == event['id']:
                    _saved = True
                    if ev['updated'] != event['updated']:
                        logger.info("Event info changed, new info:" + event['summary'])
                        ## update the message where the new event was announced
                        event['discord_msg_id'] = ev['discord_msg_id']
                        channel = self.bot.get_channel(ANNOUNCEMENTS_CHANNEL)
                        event_msg = await channel.fetch_message(event['discord_msg_id'])
                        end = event['end'].get('dateTime', event['end'].get('date'))
                        start_time_obj = dateutil_parser.parse(start) 
                        end_time_obj = dateutil_parser.parse(end) 
                        new_event_embed = discord.Embed(title=event['summary'], url=event['htmlLink'], description=f'From {start_time_obj.strftime("%d %b %Y, %H:%M")} to {end_time_obj.strftime("%d %b %Y, %H:%M")}')
                        await event_msg.edit(content=":calendar: New event added! (**updated!**)", embed=new_event_embed)
                        to_del.append(ev)
                        to_add.append(event)
            if not _saved:
                print("New event")
                msg = await self.send_calendar_embed(event, "New event added!")
                event['discord_msg_id'] = msg.id

                print(start, event['summary'])
                to_add.append(event)

        for ev in to_add:
            saved_events.append(ev)
        for ev in to_del:
            saved_events.remove(ev)

        pickle.dump(saved_events, open(CAL_EVENTS_FILE, 'wb'), protocol=0)

        now_time_obj = dateutil_parser.parse(now)
        for event in saved_events:
            start = event['start'].get('dateTime', event['start'].get('date'))
            event_time_obj = dateutil_parser.parse(start)
            time_delta_mins = (event_time_obj - now_time_obj).total_seconds() / 60
            if time_delta_mins < POLL_FREQ and time_delta_mins > 0:
                print("Upcoming event:")
                print(start, event['summary'])
                msg = await self.send_calendar_embed(event, "Event starting soon!")
            elif time_delta_mins - 1440 < POLL_FREQ and time_delta_mins - 1440 > 0:
                print("Tomorrow's event:")
                print(start, event['summary'])
                msg = await self.send_calendar_embed(event, "Event happening tomorrow!")

    @commands.command(name="events", help="List upcoming (5) calendar events")
    async def cal_upcoming(self, ctx):
        events = self.get_remote_events(5)
        if events == -1:
            channel = self.bot.get_channel(ERRORS_CHANNEL)
            await channel.send("Calendar tokens need to be refreshed!")
            return False
        events_str = ":calendar: Upcoming events:\n"
        for event in events:
            start = event['start'].get('dateTime', event['start'].get('date'))
            event_time_obj = dateutil_parser.parse(start) 
            events_str += "- " + event_time_obj.strftime("%d %b %Y, %H:%M")+ " " + event['summary'] + "\n"
        print(events_str)
        await ctx.send(events_str[:-1])

    def get_remote_events(self, n_events):
        creds = None
        # The file token.json stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists(CAL_TOKEN):
            creds = Credentials.from_authorized_user_file(CAL_TOKEN, SCOPES)
        print("Tokens file", CAL_TOKEN)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                # Send message on a specific (notifications/debug) channel 
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(CAL_CREDS, SCOPES)
                creds = flow.run_local_server(port=0)
                return -1
            # Save the credentials for the next run
            with open(CAL_TOKEN, 'w') as token:
                token.write(creds.to_json())

        service = build('calendar', 'v3', credentials=creds)
        # Call the Calendar API
        now = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
        self.logger.info("Checking calendar...  " + now)
        events_result = service.events().list(calendarId='primary', timeMin=now, \
            maxResults=n_events, singleEvents=True, orderBy='startTime').execute()
        events = events_result.get('items', [])
        return events


def setup(bot):
    bot.add_cog(CalendarCog(bot))

