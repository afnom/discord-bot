# Docker instructions

## Basic day-to-day ops

*Everytime* you make changes to the source code, to see the changes in docker,
you need to rebuild the images:

    $ docker-compose build

You can stop the bot (it might take a bit, see the docker docs for adding a
timeout if you need it):

    $ docker-compose down

To start it up:

    $ docker-compose up -d

## Maintainance

Resonable regularly, as part of a machine update, the images should be pulled
and rebuilt:

    $ docker-compose pull
    $ docker-compose build --no-cache

## Viewing the logs

To view the logs:

    $ docker-compose logs

The logs also have a follow mode, which is useful when trying to track events
in real-time:

    $ docker-compose logs -f

## Accessing containers directly

You can access the containers directly as follows:

The bot itself:

    $ docker-compose exec bot /bin/bash

The database:

    $ docker-compose exec db /bin/bash

Additionally, inside the database container, you can connect to the database
with the following command:

    $ mysql -u "$MYSQL_USER" "-p$MYSQL_PASSWORD"

