import discord
from discord.ext import commands
import logging

from bot_share import ERRORS_CHANNEL
logging.basicConfig(level=logging.INFO)

class ErrorHandler(commands.Cog):
    """A cog for global error handling."""

    def __init__(self, bot):
        self.bot = bot
        self.logger = logging.getLogger('error handler')
        self.logger.info("Added ErrorHandler")

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        """A global error handler cog."""
        if isinstance(error, commands.CommandNotFound):
            message = "Please use `++help` to see available commands"
        elif isinstance(error, commands.MissingPermissions):
            message = "You are missing the required permissions to run this command!"
        elif isinstance(error, commands.UserInputError):
            message = "Something about your input was wrong, please check your input and try again!"
        elif isinstance(error, commands.MissingRequiredArgument):
            if not type(ctx.message.channel) == discord.channel.DMChannel:
                await ctx.message.delete()
                message = "You forgot the argument! Also, the command only works if you DM me!"
            else:
                message = "You forgot the argument!"
        elif isinstance(error, commands.NoPrivateMessage):
            message = str(error)
        elif isinstance(error, commands.MissingRole):
            if not type(ctx.message.channel) == discord.channel.DMChannel:
                await ctx.message.delete()
            message = str(error)
        elif isinstance(error, discord.Forbidden):
            message = "I need to be able to DM you. Your current settings do not allow     me to! Please fix it and try again :heart:"
        else:
            message = "Oh no! Something went wrong while running the command! Please let our `root` members know. Log: " + str(error)

        await ctx.send(message)
        channel = self.bot.get_channel(ERRORS_CHANNEL)
        await channel.send("And error has occured with the bot. " + message)


def setup(bot):
    bot.add_cog(ErrorHandler(bot))
