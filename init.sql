CREATE TABLE register (
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    discordUser varchar(255),
    emailUser varchar(255),
    code varchar(255)
);

CREATE TABLE verified (
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    discordUser varchar(255),
    emailUser varchar(255)
);
