import tweepy
import os
import datetime
from dateutil import parser as dateutil_parser

import logging
import discord
from discord.ext import commands, tasks

logging.basicConfig(level=logging.INFO)
from bot_share import POLL_FREQ, ERRORS_CHANNEL, ANNOUNCEMENTS_CHANNEL
TW_APP_TOKEN = os.getenv('TW_APP_TOKEN')
TW_APP_TOKEN_SEC = os.getenv('TW_APP_TOKEN_SEC')
TW_API_KEY = os.getenv('TW_API_KEY')
TW_API_SEC = os.getenv('TW_API_SEC')


class TweetsCog(commands.Cog, name='Tweets'):
    def __init__(self, bot):
        self.bot = bot
        self.logger = logging.getLogger('tweets')
        self.tw_url = "http://twitter.com/UoB_afnom/status/"
        self.logger.info("Added TweetsCog")
        self.logger.info("Poll freq: " + str(POLL_FREQ))

    @commands.Cog.listener()
    async def on_ready(self):
        self.check_twitter.start()
        self.logger.info("Started twitter check")

    def get_tweets(self, n_tweets):
        now = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
        self.logger.info("Checking twitter..." + now)
        auth = tweepy.OAuthHandler(TW_API_KEY, TW_API_SEC)
        auth.set_access_token(TW_APP_TOKEN, TW_APP_TOKEN_SEC)

        api = tweepy.API(auth)
        
        public_tweets = api.user_timeline(tweet_mode="extended", count=n_tweets)
        return public_tweets


    @tasks.loop(minutes=POLL_FREQ)
    async def check_twitter(self):

        try:
            public_tweets = self.get_tweets(10)
        except tweepy.TweepError as e:
            self.logger.error("Tweepy Error", e)
            channel = self.bot.get_channel(ERRORS_CHANNEL)
            await channel.send("Tweepy error! " + str(e))

        now = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
        now_time = dateutil_parser.parse(now)

        ## any new tweets?
        for tweet in public_tweets:
            tweet_time = (tweet.created_at).astimezone()
            time_delta_mins = (tweet_time - now_time).total_seconds() / 60
            self.logger.info("Tweet timedelta: " + str(time_delta_mins)) 
            if abs(time_delta_mins) < POLL_FREQ:
                ## new tweet
                ## notify Discord
                
                tw_emoji = discord.utils.get(self.bot.emojis, name="tweet")
                if not tweet.retweeted:
                    new_tweet_embed = discord.Embed(title=f"{tw_emoji} New Tweet!", url=self.tw_url + tweet.id_str, description=tweet.full_text, color=1942002)
                else:
                    new_tweet_embed = discord.Embed(title=f"{tw_emoji} AFNOM retweets!", url=self.tw_url + tweet.id_str, description=tweet.full_text, color=1942002)
                channel = self.bot.get_channel(ANNOUNCEMENTS_CHANNEL)
                await channel.send(embed=new_tweet_embed)


    @commands.command(name="tweet", help="List the latest tweet from AFNOM")
    async def tweet_last(self, ctx):
        tweet = self.get_tweets(1)[0]
        tw_emoji = discord.utils.get(self.bot.emojis, name="tweet")
        new_tweet_embed = discord.Embed(title=f"{tw_emoji} Latest Tweet!", url=self.tw_url + tweet.id_str, description=tweet.full_text, color=1942002)
        await ctx.send(embed=new_tweet_embed)

def setup(bot):
    bot.add_cog(TweetsCog(bot))
