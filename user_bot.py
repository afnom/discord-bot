import os
import sys
import logging
import discord
from discord.ext import commands, tasks

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('bot')

TOKEN = os.getenv('DISCORD_TOKEN')
from bot_share import GUILD

intents = discord.Intents.default()
intents.members=True
intents.guilds=True
bot = commands.Bot('++', intents=intents)

@bot.event
async def on_error(event, *args, **kwargs):
    if event == 'on_message':
        print(f'Unhandled message: {args[0]}\n', file=sys.stderr)
    else:
        raise

@bot.event
async def on_ready():
    for guild in bot.guilds:
        if guild.name == GUILD:
            print(
                f'{bot.user} is connected to the following guild:\n'
                f'{guild.name}(id: {guild.id})\n'
            )
            break
    else:
        print(
            f'{bot.user} could not connected to the following guild:\n'
            f'{GUILD}\n'
        )

    


if __name__ == '__main__':
    bot.load_extension('calendar_cog')
    bot.load_extension('tweet_cog')
    bot.load_extension('user_cog')
    bot.load_extension('error_cog')
    bot.run(TOKEN)
