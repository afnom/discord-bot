import hashlib
import logging
import random
import smtplib
import ssl
import string
import os
from re import match
from socket import gaierror
import requests
import discord
from discord.ext import commands, tasks
from db_connect import connect_db
logging.basicConfig(level=logging.INFO)
from bot_share import ERRORS_CHANNEL, ANNOUNCEMENTS_CHANNEL, GUILD

class UserCog(commands.Cog, name="User verification"):
    def __init__(self, bot):
        self.bot = bot
        self.logger = logging.getLogger('user')
        self.logger.info("Added UserCog")
        self.msg_verify = discord.Embed(title="Process", description="We will use your \
*University of Birmingham* student email to verify you are a student \
here!\nFirst, we need your *student username*. It should be of the form \
*abc123*. To start the authentication process, please supply **your** username \
to the command `++emailuser abc123`. We will send an email to your address \
(abc123@bham.ac.uk).\n**If you are an alumni, please use the command \
`++emailalumni` instead!**", color=0x0ce3ac)

        self.msg_username = discord.Embed(description="We have sent a code to the email \
mentioned above. Please use the command `++code xxxx-yyyy-zzzz` where \
`xxxx-yyyy-zzzz` is the code you received in your email. \
Make sure to check your junk folder, as the emails often end up there!", color=0x0ce3ac)

        self.msg_role_upgraded = discord.Embed(description="**verified** role applied", color=0x0ce3ac)

        self.msg_code_incorrect = discord.Embed(description="The code provided is incorrect. \
Please provide the code we send on your email. The code is case sensitive and \
needs to be in the form `xxxx-yyyy-zzzz`.", color=0xff0000)

        self.msg_email_in_use = discord.Embed(description="The email user you provided is \
already registered. Please provide *your own* UoB email user. If you believe \
this is an error, please contact one of our `root` role members.", color=0xff0000)
        self.code_base = string.ascii_letters + string.digits

    def get_hash(self, msg):
        m = hashlib.sha256()
        m.update(str.encode(msg))
        m = (m.digest()).hex()
        return m

    def gen_code(self):
        code = ''.join(random.choice(self.code_base) for i in range(12))
        return code[:4] + '-' + code[4:8] + '-' + code[8:]

    def send_email(self, receiver, code):
        email= f"""\
Subject: AFNOM Discord Server Verification
To: {receiver}
From: {os.getenv('SENDER_EMAIL')}

Hi! This is your verification code: {code} \nPlease continue the verification process on Discord by supplying this code to our bot."""
        try:
            context = ssl.create_default_context()
            #send your message with credentials specified above
            with smtplib.SMTP_SSL(os.getenv('SMTP_SERVER'), int(os.getenv('SMTP_PORT')), context=context) as server:
                server.login(os.getenv('SMTP_LOGIN'), os.getenv('SMTP_PASS'))
                server.sendmail(os.getenv('SENDER_EMAIL'), receiver, email)

            # tell the script to report if your message was sent or which errors need to be fixed 
            return True
        except (gaierror, ConnectionRefusedError):
            print('Failed to connect to the server. Bad connection settings?')
            return False
        except smtplib.SMTPServerDisconnected:
            print('Failed to connect to the server. Wrong user/password?')
            return False
        except smtplib.SMTPException as e:
            print('SMTP error occurred: ' + str(e))
            return False

    async def hook_username(self, ctx, keyword, user_type):
        if not type(ctx.message.channel) == discord.channel.DMChannel:
            await ctx.message.delete()
            await ctx.send("This command only works if you DM me!")
        else:
            user_hash = self.get_hash(ctx.message.author.name + '#' + ctx.message.author.discriminator)
            conn = connect_db()
            cur = conn.cursor()
            cur.execute('SELECT discordUser FROM verified WHERE discordUser=?', (user_hash,))
            result = cur.fetchone()
            if not result is None:
                guild = None
                for guild in self.bot.guilds:
                    if guild.name == GUILD:
                        break
                if guild is None:
                    await ctx.send("Could not get Bot Guild. Please contact one of our `root` members")
                    channel = self.bot.get_channel(ERRORS_CHANNEL)
                    await channel.send("could not get bot guild!!")
                member = guild.get_member(ctx.message.author.id)
                role = discord.utils.get(guild.roles, name="verified")
                await member.add_roles(role);
                await ctx.send("You have already been verified.", embed=self.msg_role_upgraded)
                conn.close()
                return 0

            email_hash = self.get_hash(keyword.lower())
            cur.execute('SELECT discordUser, emailUser FROM verified WHERE emailUser=?', (email_hash,))
            result = cur.fetchone()
            if not result is None:
                await ctx.send(embed=self.msg_email_in_use)
            else:
                cur.execute('SELECT discordUser, emailUser FROM register WHERE discordUser=?', (user_hash,))
                result = cur.fetchone()
                if result is None:

                    if not type(ctx.message.channel) == discord.channel.DMChannel:
                        await ctx.message.delete()
                        await ctx.send("The `emailuser` command only works if you DM me!")
                    else:
                        embed = self.msg_username
                        
                        if user_type == "alumni": 
                            receiver = keyword + "@alumni.bham.ac.uk"
                        else:
                            receiver = keyword + "@bham.ac.uk"
                        if not match("^[a-zA-Z0-9.]+$", keyword):
                            await ctx.send(f"Invalid Email Address: {receiver}")
                            return 0
                        embed.title = receiver
                        code = self.gen_code()
                        if not self.send_email(receiver, code):
                            await ctx.message.author.send("There was an error sending the email! Please let one of our `root` members know.")
                        else:
                            ## add to DB
                            try: 
                                cur.execute("INSERT INTO register (discordUser, emailUser, code) VALUES (?, ?, ?)", (user_hash, email_hash, code)) 
                            except mariadb.Error as e:
                                await ctx.message.author.send("There was an error in our databse. Please let one of our `root` members know.\nError: "+ str(e))                    
                                channel = self.bot.get_channel(ERRORS_CHANNEL)
                                await channel.send("Error inserting into DB.")

                            conn.commit() 
                            await ctx.message.author.send("Excellent!", embed=embed)
                else:
                    if not type(ctx.message.channel) == discord.channel.DMChannel:
                        await ctx.message.delete()
                    await ctx.message.author.send("You have already started the verification process. Please use the `++code` command to tell me the verification code sent on your email.")

            conn.close()


    @commands.has_role("guest")
    @commands.command(name='verify', pass_context=True, help="Start the verification process. This command only works on a server channel.")
    async def init_verify(self, ctx):
        hash_user = self.get_hash(ctx.message.author.name + '#' + ctx.message.author.discriminator)
        conn = connect_db()
        cur = conn.cursor()
        cur.execute('SELECT discordUser, emailUser FROM verified WHERE discordUser=?', (hash_user,))
        result = cur.fetchone()
        conn.close()
        if result is None:
            if not type(ctx.message.channel) == discord.channel.DMChannel:
                await ctx.message.delete()
            await ctx.message.author.send("Hello, welcome to the AFNOM Discord! I will \
    help you through the verification process.", embed=self.msg_verify)
        else:
            if not type(ctx.message.channel) == discord.channel.DMChannel:
                await ctx.message.delete()
            guild = None
            for guild in self.bot.guilds:
                if guild.name == GUILD:
                    break
            if guild is None:
                await ctx.send("Could not get Bot Guild. Please contact one of our `root` members")
            member = guild.get_member(ctx.message.author.id)
            role = discord.utils.get(guild.roles, name="verified")
            await member.add_roles(role);
            await ctx.send("You have already been verified.", embed=self.msg_role_upgraded)

    @commands.command(name='emailuser', pass_context=True, help="Provide your UoB email username, for verification purposes. This command only works in a DM with the bot.")
    async def email_user(self, ctx, keyword):
        await self.hook_username(ctx, keyword, 'current')

    @commands.command(name='emailalumni', pass_context=True, help="Provide your UoB email username, for verification purposes. This command only works in a DM with the bot.")
    async def email_alumni(self, ctx, keyword):
        await self.hook_username(ctx, keyword, 'alumni')

    @commands.command(name="code", pass_context=True, help="Provide the code that was sent to your email. This command only works in a DM with the bot.")
    async def auth_username(self, ctx, keyword):
        if not type(ctx.message.channel) == discord.channel.DMChannel:
            await ctx.message.delete()
            await ctx.send("This command only works if you DM me!")

        else:
            user_hash = self.get_hash(ctx.message.author.name + '#' + ctx.message.author.discriminator)
            conn = connect_db()
            cur = conn.cursor()
            cur.execute('SELECT discordUser, emailUser, code FROM register WHERE discordUser=?', (user_hash,))
            result = cur.fetchone()
            if result is None:
                await ctx.message.author.send("You have not yet provided an email username. \
        Please use the `++emailuser` command, such that we can send you the authentication code.")
            else:
                db_code = (result[2])
                if db_code == keyword:
                    guild = None
                    for guild in self.bot.guilds:
                        if guild.name == GUILD:
                            break
                    if guild is None:
                        await ctx.message.author.send("Could not get Bot Guild. Please contact one of our `root` members")
                    else:
                        member = guild.get_member(ctx.message.author.id)
                        role = discord.utils.get(guild.roles, name="verified")
                        await member.add_roles(role);
                        try:
                            cur.execute("INSERT INTO verified(discordUser, emailUser) VALUES (?, ?)", (user_hash, result[1])) 
                        except mariadb.Error as e:
                            await ctx.message.author.send("There was an error in our databse. Please let one \
        of our `root` members know.\nError: "+ str(e))                    

                        conn.commit() 
                        try:
                            cur.execute("DELETE FROM register WHERE discordUser=? AND emailUser=?", (user_hash, result[1])) 
                        except mariadb.Error as e:
                            await ctx.message.author.send("There was an error in our databse. Please let one \
        of our `root` members know.\nError: "+ str(e))                    

                        conn.commit() 
                        await ctx.message.author.send("You have successfuly been verified.", embed=self.msg_role_upgraded)
                else:
                    ## codes don't match
                    await ctx.message.author.send(embed=self.msg_code_incorrect)

            conn.close()

    @commands.command(name='delete', pass_context=True, help="Delete your username from the registration and verification databases. This command only works in a DM with the bot. Use this if you gave the bot the wrong email user.")
    async def delete_user(self, ctx):

        if not type(ctx.message.channel) == discord.channel.DMChannel:
            await ctx.message.delete()
            await ctx.send("This command only works if you DM me!")
        else:
            user_hash = self.get_hash(ctx.message.author.name + '#' + ctx.message.author.discriminator)
            conn = connect_db()
            cur = conn.cursor()
            cur.execute('SELECT discordUser FROM verified WHERE discordUser=?', (user_hash,))
            result = cur.fetchone()
            if result is None:
                await ctx.send("You are not in the **verified** database.")
            else:
                cur.execute('DELETE FROM verified WHERE discordUser=?', (user_hash,))
                conn.commit() 
                await ctx.send("You have been removed from the **verified** database.")
                
            cur.execute('SELECT discordUser, emailUser FROM register WHERE discordUser=?', (user_hash,))
            result = cur.fetchone()
            if result is None:
                await ctx.send("You are not in the **pending registration** database.")
            else:
                cur.execute('DELETE FROM register WHERE discordUser=?', (user_hash,))
                conn.commit() 
                await ctx.send("You have been removed from the **pending registration** database.")

            conn.close()


def setup(bot):
    bot.add_cog(UserCog(bot))
